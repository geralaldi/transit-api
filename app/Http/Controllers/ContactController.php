<?php namespace App\Http\Controllers;

use App\Contacts;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent;
use DB;

class ContactController extends Controller
{

    /**
     * Show the profile for the given user.
     *
     * @param  int $id
     * @return Response
     */


    public function showContacts()
    {
        $contacts = DB::table('contacts')->get();
        $contacts = Contacts::all();
        // $result = array();
        // foreach ($contacts as $contact) {
        // array_push($result, $contact);
        // }
        // return response()->json([$result]);
        return response()->json([
            'status' => true,
            'list' => $contacts
        ]);
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: geraldi
 * Date: 5/27/15
 * Time: 1:44 AM
 */

namespace app\Http\Controllers;

use App\Http\Controllers\Controller;
use DB;
use Request;


class PlaceController extends Controller {

    public function findNearbyPlaces() //list of nearby area
    {
        $keyname = Request::input('keyword');

        $keyname = urlencode($keyname);

        $around = file_get_contents('https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-7.801859,110.369137&radius=70000&keyword=' . $keyname . '&key=AIzaSyCDJvHCx4yEfnAkFu4qZoSa0UCyIWl08aA');

        $around = json_decode($around);

        $finres = array();
        foreach ($around->results as $place) {

            $lokasilat = $place->geometry->location->lat;
            $lokasilng = $place->geometry->location->lng;
            $name = $place->name;
            $vicinity = $place->vicinity;

            $jsonres = array(
                'latitude' => $lokasilat,
                'longitude' => $lokasilng,
                'name' => $name,
                'vicinity' => $vicinity
            );

            array_push($finres, $jsonres);

        }


        return response()->json(['places' => $finres]);
    }


}
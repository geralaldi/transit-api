<?php namespace App\Http\Controllers;

use App\Routes;
use App\Halte;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent;
use DB;
use Request;

class RouteController extends Controller
{

    /**
     * Show the profile for the given user.
     *
     * @param  int $id
     * @return Response
     */

    #function time now


    public function setDuration()
    {
        $sql = DB::select('select id from routes');
        $result = array();
        $latFrom = array();
        $latTo = array();
        $longFrom = array();
        $longTo = array();


        foreach ($sql as $key => $value) {
            $result[] = $value->id;
        }

        foreach ($result as $id => $value) {
            $DBfrom = DB::select('select routes.id, latitude, longitude from halte, routes where halte.halte= routes.halte');
            $DBto = DB::select('select routes.id, latitude, longitude from halte, routes where halte.halte= routes.to');
            $DBid = DB::select('select id from routes');

            // print_r($from);

            // $halte = Routes::all();

        }

        $id = array();
        $f = json_decode(json_encode($DBfrom), true);
        $t = json_decode(json_encode($DBto), true);
        // var_dump($t);
        foreach ($f as $key => $value) {

            foreach ($t as $keyT => $value) {
                $Tolatitude = $t[$keyT]['latitude'];
                $Tolongitude = $t[$keyT]['longitude'];

                if ($f[$key]['id'] == $t[$keyT]['id']) {
                    $f[$key]['Tolatitude'] = $Tolatitude;
                    $f[$key]['Tolongitude'] = $Tolongitude;
                }
            }
        }


        foreach ($f as $key => $value) {
            $direction = file_get_contents('https://maps.googleapis.com/maps/api/directions/json?origin=' . $value['latitude'] . ',' . $value['longitude'] . '&destination=' . $value['Tolatitude'] . ',' . $value['Tolongitude'] . '&key=AIzaSyCG7JQVc1l8TpJUb1qWxtb9rvB2YnPo7n4');

            // $string = 'https://maps.googleapis.com/maps/api/directions/json?origin='.$value['latitude'].','.$value['longitude'].'&destination='.$value['Tolatitude'].','.$value['Tolongitude'].'&key=AIzaSyCG7JQVc1l8TpJUb1qWxtb9rvB2YnPo7n4';

            $distance = json_decode($direction, true)['routes'][0]['legs'][0]['distance']['value'];
            $duration = json_decode($direction, true)['routes'][0]['legs'][0]['duration']['value'];


            $routes = Routes::find($value['id']);

            $routes->jarak = $distance;
            $routes->waktu = $duration;

            $routes->save();

        }

        // $direction=json_decode($direction,true);

    }

    public function routesbyTaxi()
    {
        $Fromlatitude = Request::input('latitude'); // BACA INPUT POSISI USER : TYPE Latitude
        $Fromlongitude = Request::input('longitude'); // BACA INPUT POSISI USER : TYPE Longitude
        $Tolatitude = Request::input('tolatitude'); // BACA INPUT TUJUAN USER : TYPE Latitude
        $Tolongitude = Request::input('tolongitude'); // BACA INPUT TUJUAN USER : TYPE Longitude

        $direction = file_get_contents('https://maps.googleapis.com/maps/api/directions/json?origin=' . $Fromlatitude . ',' . $Fromlongitude . '&destination=' . $Tolatitude . ',' . $Tolongitude . '&key=AIzaSyCG7JQVc1l8TpJUb1qWxtb9rvB2YnPo7n4');

        // $string = 'https://maps.googleapis.com/maps/api/directions/json?origin='.$value['latitude'].','.$value['longitude'].'&destination='.$value['Tolatitude'].','.$value['Tolongitude'].'&key=AIzaSyCG7JQVc1l8TpJUb1qWxtb9rvB2YnPo7n4';

        $distance = json_decode($direction, true)['routes'][0]['legs'][0]['distance']['value'];
        $duration = json_decode($direction, true)['routes'][0]['legs'][0]['duration']['value'];
        $cost = ($distance / 1000) * 4000;

        $taxi = array(
            'duration' => $duration,
            'type' => 'taxi',
            'distance' => $distance,
            'cost' => 'Rp.' . $cost . ',00'
        );

        echo json_encode($taxi);
    }

    public function routesbyTrans()
    {
        $HalteTransit = array();
        $Fromlatitude = Request::input('latitude'); // BACA INPUT POSISI USER : TYPE Latitude
        $Fromlongitude = Request::input('longitude'); // BACA INPUT POSISI USER : TYPE Longitude
        $Tolatitude = Request::input('tolatitude'); // BACA INPUT TUJUAN USER : TYPE Latitude
        $Tolongitude = Request::input('tolongitude'); // BACA INPUT TUJUAN USER : TYPE Longitude

        $selfaddress = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $Fromlatitude . ',' . $Fromlongitude . '&key=AIzaSyCG7JQVc1l8TpJUb1qWxtb9rvB2YnPo7n4');
        $togoaddress = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $Tolatitude . ',' . $Tolongitude . '&key=AIzaSyCG7JQVc1l8TpJUb1qWxtb9rvB2YnPo7n4');

        if (strcmp(json_decode($selfaddress, true)['results'][0]['address_components'][0]['types'][0], 'route') == 0) {
            $selfpos = json_decode($selfaddress, true)['results'][0]['address_components'][0]['long_name'];
            $spnumber = '';
        } else {
            $spnumber = json_decode($selfaddress, true)['results'][0]['address_components'][0]['long_name'];
            $selfpos = json_decode($selfaddress, true)['results'][0]['address_components'][1]['long_name'];
        }

        if (strcmp(json_decode($togoaddress, true)['results'][0]['address_components'][0]['types'][0], 'route') == 0) {
            $togopos = json_decode($togoaddress, true)['results'][0]['address_components'][0]['long_name'];
            $tgpnumber = '';
        } else {
            $tgpnumber = json_decode($togoaddress, true)['results'][0]['address_components'][0]['long_name'];
            $togopos = json_decode($togoaddress, true)['results'][0]['address_components'][1]['long_name'];
        }


        // $selfaddress adalah alamat posisi user saat itu


        $earthMeanRadius = 6371;
        $haltefrom = array();
        $halteto = array();
        $minimum = 9999;
        $tominimum = 9999;
        $duration = 0;
        $transToDur = 0;
        $fromTransDur = 0;


        $DBcoor = DB::select('select * from halte');

        $coor = json_decode(json_encode($DBcoor), true);

        foreach ($coor as $key) {

            $latitudeTo = $key['latitude'];
            $longitudeTo = $key['longitude'];

            $deltaLatitude = deg2rad($latitudeTo - $Fromlatitude);
            $deltaLongitude = deg2rad($longitudeTo - $Fromlongitude);
            $deltatoLatitude = deg2rad($latitudeTo - $Tolatitude);
            $deltatoLongitude = deg2rad($longitudeTo - $Tolongitude);

            $a = sin($deltaLatitude / 2) * sin($deltaLatitude / 2) +
                cos(deg2rad($Fromlatitude)) * cos(deg2rad($latitudeTo)) *
                sin($deltaLongitude / 2) * sin($deltaLongitude / 2);
            $c = 2 * atan2(sqrt($a), sqrt(1 - $a));

            $b = sin($deltatoLatitude / 2) * sin($deltatoLatitude / 2) +
                cos(deg2rad($Tolatitude)) * cos(deg2rad($latitudeTo)) *
                sin($deltatoLongitude / 2) * sin($deltatoLongitude / 2);
            $d = 2 * atan2(sqrt($b), sqrt(1 - $b));

            $distance = $earthMeanRadius * $c;
            $todistance = $earthMeanRadius * $d;


            if ($distance < $minimum) {
                $haltefrom = array($key['id'], $key['halte'], $key['latitude'], $key['longitude']);
                $minimum = $distance;
            }

            if ($todistance < $tominimum) {
                $halteto = array($key['id'], $key['halte'], $key['latitude'], $key['longitude']);
                $tominimum = $todistance;
            }

            //haltefrom = halte tempat terdekat dari posisi awal user
            //halteto = halte terdekat dari tujuan user

        }


        $haltefaddress = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $haltefrom[2] . ',' . $haltefrom[3] . '&key=AIzaSyCG7JQVc1l8TpJUb1qWxtb9rvB2YnPo7n4');
        $haltetaddress = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $halteto[2] . ',' . $halteto[3] . '&key=AIzaSyCG7JQVc1l8TpJUb1qWxtb9rvB2YnPo7n4');

        if (strcmp(json_decode($haltefaddress, true)['results'][0]['address_components'][0]['types'][0], 'route') == 0) {
            $streethaltefrompos = json_decode($haltefaddress, true)['results'][0]['address_components'][0]['long_name'];
            $streethaltefromnumber = '';
        } else {
            $streethaltefromnumber = json_decode($haltefaddress, true)['results'][0]['address_components'][0]['long_name'];
            $streethaltefrompos = json_decode($haltefaddress, true)['results'][0]['address_components'][1]['long_name'];
        }
        //$streetpos adalah alamat jalan tempat halte terdekat ke posisi user

        if (strcmp(json_decode($haltetaddress, true)['results'][0]['address_components'][0]['types'][0], 'route') == 0) {
            $streethaltetopos = json_decode($haltetaddress, true)['results'][0]['address_components'][0]['long_name'];
            $streethaltetonumber = '';
        } else {
            $streethaltetonumber = json_decode($haltetaddress, true)['results'][0]['address_components'][0]['long_name'];
            $streethaltetopos = json_decode($haltetaddress, true)['results'][0]['address_components'][1]['long_name'];
        }

        $IdHalteFrom = $haltefrom[0];
        $IdHalteTo = $halteto[0];


        $firsttohaltedir = file_get_contents('https://maps.googleapis.com/maps/api/directions/json?origin=' . $Fromlatitude . ',' . $Fromlongitude . '&destination=' . $haltefrom[2] . ',' . $haltefrom[3] . '&key=AIzaSyCG7JQVc1l8TpJUb1qWxtb9rvB2YnPo7n4');
        $haltetolastdir = file_get_contents('https://maps.googleapis.com/maps/api/directions/json?origin=' . $halteto[2] . ',' . $halteto[3] . '&destination=' . $Tolatitude . ',' . $Tolongitude . '&key=AIzaSyCG7JQVc1l8TpJUb1qWxtb9rvB2YnPo7n4');
        $firsttohaltedist = json_decode($firsttohaltedir, true)['routes'][0]['legs'][0]['distance']['value'];
        $haltetolastdist = json_decode($haltetolastdir, true)['routes'][0]['legs'][0]['distance']['value'];
        $firsttohaltedur = json_decode($firsttohaltedir, true)['routes'][0]['legs'][0]['duration']['value'];
        $haltetolastdur = json_decode($haltetolastdir, true)['routes'][0]['legs'][0]['duration']['value'];


        $HFTrayek = array();
        $HTTrayek = array();
        $NoTransit = array(array());
        $val = array();
        $transit = array(array());
        $qualified = array();
        $SelectedTrayek;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        $DBHalteFrom = DB::select('select * from routes where halte in (select halte from halte where id=?)', [$IdHalteFrom]);
        $DBHalteTo = DB::select('select * from routes where `to` in (select halte from halte where id=?)', [$IdHalteTo]);

        $HalteFrom = json_decode(json_encode($DBHalteFrom), true);
        $HalteTo = json_decode(json_encode($DBHalteTo), true);


        foreach ($HalteFrom as $key) {
            array_push($HFTrayek, $key['trayek']);
        }
        foreach ($HalteTo as $key) {
            array_push($HTTrayek, $key['trayek']);
        }
        $intersect = array_intersect($HFTrayek, $HTTrayek);

        if (empty($intersect)) {

            foreach ($HalteFrom as $selectedtf) {
                foreach ($HalteTo as $selectedtt) {
                    $trayekfrom = $selectedtf['trayek'];
                    $trayekto = $selectedtt['trayek'];
                    $idfrom = $selectedtf['id'];
                    $idto = $selectedtt['id'];
                    $halteFrom = $selectedtf['halte'];
                    $halteTo = $selectedtt['to'];

                    $DBtransitTF = DB::select('select id,halte,`to` FROM `routes` WHERE trayek=? and (halte) in (select halte FROM `routes` WHERE trayek=?)', [$trayekto, $trayekfrom]);
                    $DBtransitFT = DB::select('select id,halte,`to` FROM `routes` WHERE trayek=? and (halte) in (select halte FROM `routes` WHERE trayek=?)', [$trayekfrom, $trayekto]);

                    $transitTF = json_decode(json_encode($DBtransitTF), true);
                    $transitFT = json_decode(json_encode($DBtransitFT), true);


                    foreach ($transitTF as $key => $value) {
                        $transitTF[$key]['idFrom'] = $idfrom;
                        $transitTF[$key]['halteFrom'] = $halteFrom;
                        $transitTF[$key]['trayekFrom'] = $trayekfrom;
                        $transitTF[$key]['idTo'] = $idto;
                        $transitTF[$key]['halteTo'] = $halteTo;
                        $transitTF[$key]['trayekTo'] = $trayekto;
                    }

                    foreach ($transitFT as $key => $value) {
                        $transitFT[$key]['idFrom'] = $idfrom;
                        $transitFT[$key]['halteFrom'] = $halteFrom;
                        $transitFT[$key]['trayekFrom'] = $trayekfrom;
                        $transitFT[$key]['idTo'] = $idto;
                        $transitFT[$key]['halteTo'] = $halteTo;
                        $transitFT[$key]['trayekTo'] = $trayekto;

                    }

                    foreach ($transitFT as $FT) {
                        foreach ($transitTF as $TF) {
                            if (strcmp($TF['halte'], $FT['halte'] == 0)) {

                                if (($FT['id'] > $selectedtf['id']) and ($TF['id'] < $selectedtt['id'])) {

                                    array_push($qualified, array($FT['id'], $FT['halte'], $FT['idFrom'], $FT['halteFrom'], $FT['trayekFrom'], $FT['idTo'], $FT['halteTo'], $FT['trayekTo']));
                                }
                            }
                        }
                    }


                }
            }
            $qualified = array_map("unserialize", array_unique(array_map("serialize", $qualified)));


            $minimum = 1000;
            $IDterdekat = 0;

            foreach ($HalteFrom as $keyF) {
                foreach ($qualified as $key) {
                    $idfrom = $keyF['id'];
                    $tes = $key[0];
                    $selisih = $tes - $idfrom;


                    if (($selisih < $minimum) and ($selisih > 0)) {
                        $minimum = $selisih;
                        $IDterdekat = $tes;

                    }

                }

            }


            foreach ($qualified as $key) {
                if ($key[0] == $IDterdekat) {
                    $HalteTransit = array($key[0], $key[1], $key[2], $key[3], $key[4], $key[5], $key[6], $key[7]);

                }

            }


            $DBallHalte = DB::select('select * from routes');
            $allHalte = json_decode(json_encode($DBallHalte), true);

            $halte = $HalteTransit[1];
            $trayekTo = $HalteTransit[7];
            $idFrom = $HalteTransit[2];
            $idTo = $HalteTransit[5];
            $idTransitF = $HalteTransit[0];
            $DBidTransitT = DB::select('select id from routes where halte =? and trayek=?', [$halte, $trayekTo]);
            $idTransitT = json_decode(json_encode($DBidTransitT), true);
            $idTransit = $idTransitT[0]['id'];
            $halteTo = $HalteTransit[6];


            $DBcoorTransit = DB::select('select * from halte where halte=?', [$halte]);
            $coorTransit = json_decode(json_encode($DBcoorTransit), true);
            $transitLat = $coorTransit[0]['latitude'];
            $transitLong = $coorTransit[0]['longitude'];


            if ($idTo < $idTransit) {
                $DBganti = DB::select('select id from routes where `to`=? and trayek=?', [$halteTo, $trayekTo]);
                $ganti = json_decode(json_encode($DBganti), true);

                $idToAlt = $ganti[0]['id'];


                $DBfromTransit = DB::select('select id, halte, trayek, jarak, waktu from routes where id>=? and id<=?', [$idFrom, $idTransitF]);
                $DBtransitTo = DB::select('select id, halte, trayek, jarak, waktu from routes where id>? and id<=?', [$idTransit, $idToAlt]);
                $fromTransit = json_decode(json_encode($DBfromTransit), true);
                $transitTo = json_decode(json_encode($DBtransitTo), true);
                // var_dump($transitTo);


                foreach ($fromTransit as $key) {
                    $duration = $duration + $key['waktu'];
                    $fromTransDur = $duration;

                }
                $duration = 0;
                foreach ($transitTo as $key) {
                    $duration = $duration + $key['waktu'];
                    $transToDur = $duration;

                }

            } else {
                $DBfromTransit = DB::select('select id, halte,`to`, trayek, jarak, waktu from routes where id>=? and id<=?', [$idFrom, $idTransitF]);
                $DBtransitTo = DB::select('select id, halte,`to`, trayek, jarak, waktu from routes where id>? and id<=?', [$idTransit, $idTo]);

                $fromTransit = json_decode(json_encode($DBfromTransit), true);
                $transitTo = json_decode(json_encode($DBtransitTo), true);


                foreach ($fromTransit as $key) {
                    $duration = $duration + $key['waktu'];
                    $fromTransDur = $duration;

                }
                $duration = 0;

                foreach ($transitTo as $key) {
                    $duration = $duration + $key['waktu'];
                    $transToDur = $duration;
                }
            }


            $HalteHalte = array_merge($fromTransit, $transitTo);
            $totalDuration = $firsttohaltedur + $fromTransDur + $haltetolastdur + $transToDur;


            $trip = array(
                'duration' => $totalDuration,
                'type' => 'public',
                'detail' => array(
                    array(
                        'from' => [
                            'address' => $selfpos . ' ' . $spnumber,
                            'lat' => $Fromlatitude,
                            'lng' => $Fromlongitude
                        ],
                        'to' => [
                            'address' => $streethaltefrompos . ' ' . $streethaltefromnumber,
                            'lat' => $haltefrom[2],
                            'lng' => $haltefrom[3]
                        ],
                        'action' => 'Walk',
                        'notes' => $firsttohaltedist . ' m',
                        'duration' => $firsttohaltedur,
                        'identifier' => 'w_walk'
                    ),
                    array(
                        'from' => [
                            'address' => 'Halte ' . $HalteTransit[3],
                            'lat' => $haltefrom[2],
                            'lng' => $haltefrom[3]
                        ],
                        'to' => [
                            'address' => 'Halte ' . $HalteTransit[1],
                            'lat' => $transitLat,
                            'lng' => $transitLong
                        ],
                        'action' => 'Take ' . $HalteTransit[4],
                        'duration' => $fromTransDur,
                        'identifier' => 'take_halte'
                    ),
                    array(
                        'from' => [
                            'address' => 'Halte ' . $HalteTransit[1],
                            'lat' => $transitLat,
                            'lng' => $transitLong
                        ],
                        'to' => [
                            'address' => 'Halte ' . $HalteTransit[6],
                            'lat' => $halteto[2],
                            'lng' => $halteto[3]
                        ],
                        'action' => 'Take ' . $HalteTransit[7],
                        'duration' => $transToDur,
                        'identifier' => 'take_halte'
                    ),
                    array(
                        'from' => [
                            'address' => $streethaltetopos . ' ' . $streethaltetonumber,
                            'lat' => $halteto[2],
                            'lng' => $halteto[3]
                        ],
                        'to' => [
                            'address' => $togopos . ' ' . $tgpnumber,
                            'lat' => $Tolatitude,
                            'lng' => $Tolongitude
                        ],
                        'action' => 'Walk',
                        'notes' => $haltetolastdist . ' m',
                        'duration' => $haltetolastdur,
                        'identifier' => 'w_walk'
                    )
                )
            );

        } else {


            $SelectedTrayek = $intersect[1];
            $DBHalteFrom = DB::select('select * from routes where halte in (select halte from halte where id=? and trayek=?)', [$IdHalteFrom, $SelectedTrayek]);
            $DBHalteTo = DB::select(' select * from routes where halte in (select halte from halte where id=? and trayek=?)', [$IdHalteTo, $SelectedTrayek]);
            $HalteFrom = json_decode(json_encode($DBHalteFrom), true);
            $HalteTo = json_decode(json_encode($DBHalteTo), true);

            $IDF = $HalteFrom[0]['id'];
            $IDT = $HalteTo[0]['id'];
            $DBHalteHalte = DB::select('select id, halte, trayek, waktu from routes where id >=? and id<=? ', [$IDF, $IDT]);
            $HalteHalte = json_decode(json_encode($DBHalteHalte), true);

            foreach ($HalteHalte as $key) {
                $duration = $duration + $key['waktu'];
            }
            $totalDuration = $firsttohaltedur + $duration + $haltetolastdur;
            $trip = array(
                'duration' => $totalDuration,
                'type' => 'public',
                'detail' => array(
                    array(
                        'from' => [
                            'address' => $selfpos . ' ' . $spnumber,
                            'lat' => $Fromlatitude,
                            'lng' => $Fromlongitude
                        ],
                        'to' => [
                            'address' => $streethaltefrompos . ' ' . $streethaltefromnumber,
                            'lat' => $haltefrom[2],
                            'lng' => $haltefrom[3]
                        ],
                        'action' => 'Walk',
                        'notes' => $firsttohaltedist . ' m',
                        'duration' => $firsttohaltedur,
                        'identifier' => 'w_walk'
                    ),
                    array(
                        'from' => [
                            'address' => 'Halte ' . $HalteFrom[0]['halte'],
                            'lat' => $haltefrom[2],
                            'lng' => $haltefrom[3]
                        ],
                        'to' => [
                            'address' => 'Halte ' . $HalteTo[0]['halte'],
                            'lat' => $halteto[2],
                            'lng' => $halteto[3]
                        ],
                        'action' => 'Take ' . $HalteFrom[0]['trayek'],
                        'duration' => $duration,
                        'identifier' => 'take_halte'
                    ),
                    array(
                        'from' => [
                            'address' => $streethaltetopos . ' ' . $streethaltetonumber,
                            'lat' => $halteto[2],
                            'lng' => $halteto[3]
                        ],
                        'to' => [
                            'address' => $togopos . ' ' . $tgpnumber,
                            'lat' => $Tolatitude,
                            'lng' => $Tolongitude
                        ],
                        'action' => 'Walk',
                        'notes' => $haltetolastdist . ' m',
                        'duration' => $haltetolastdur,
                        'identifier' => 'w_walk'
                    )
                )
            );
        }


//        echo json_encode($trip);
        return response()->json(['route' => $trip]);
    }

    public function lookAround() //list of nearby area
    {
        $keyname = Request::input('keyword');

        $keyname = urlencode($keyname);

        $around = file_get_contents('https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-7.801859,110.369137&radius=70000&keyword=' . $keyname . '&key=AIzaSyCDJvHCx4yEfnAkFu4qZoSa0UCyIWl08aA');

        $around = json_decode($around);

        // $res1 = array();

        // foreach ($around->results as $key => $value) 
        // {
        //      array_push($res1, $key);
        // }

        // $res2 = array();

        // foreach ($res1 as $key => $value) {
        //  // array_push($res2, $around->results[$key]->geometry->location->lat);
        //  // array_push($res2, $around->results[$key]->geometry->location->lng);
        //  array_push($res2, $around->results[$key]->geometry->location);
        //  array_push($res2, $around->results[$key]->name);
        //  array_push($res2, $around->results[$key]->vicinity);

        // }

        // foreach ($res2 as $key => $value) {
        //  echo $value;
        # code...
        // }

        // return response()->json($res2);

        // return response()->json($around->results);
        $finres = array();
        foreach ($around->results as $key => $value) {

            $lokasilat = $around->results[$key]->geometry->location->lat;
            $lokasilng = $around->results[$key]->geometry->location->lng;
            $name = $around->results[$key]->name;
            $vicinity = $around->results[$key]->vicinity;

            $jsonres = array(
                'lat' => $lokasilat,
                'lng' => $lokasilng,
                'name' => $name,
                'vicinity' => $vicinity
            );

            array_push($finres, $jsonres);

        }


        return response()->json(['nearbylocation' => $finres]);


    }


}
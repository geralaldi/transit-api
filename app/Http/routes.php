<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$app->get('/', function() use ($app) {
    return $app->welcome();
});

$app->get('/contacts', 'App\Http\Controllers\ContactController@showContacts');

$app->post('/routes', 'App\Http\Controllers\RouteController@findRoutes');

$app->get('/dist', 'App\Http\Controllers\RouteController@setDuration');

$app->post('/reverse', 'App\Http\Controllers\RouteController@reverseGeocode');

$app->post('/near', 'App\Http\Controllers\RouteController@haversineGreatCircleDistance');

$app->post('/route/public', 'App\Http\Controllers\RouteController@routesbyTrans');

$app->post('/route/taxi', 'App\Http\Controllers\RouteController@routesbyTaxi');

$app->get('/places','App\Http\Controllers\PlaceController@findNearbyPlaces');


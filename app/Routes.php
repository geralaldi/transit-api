<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Routes extends Model {

    protected $table = 'routes';
    protected $fillable = ['Jarak', 'Waktu'];
    public $timestamps = false;
    

}

class Halte extends Model {

	protected $table = 'halte';

}

?>